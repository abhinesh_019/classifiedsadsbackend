const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({
 
     mainTitle:String,
    subtitle:String,
    description:String, 
    links:String,
    image:String
},
{timestamps:true});

const parlourArea=mongoose.model('classifiedsBlogs',parlourSchemaAreas);
module.exports=parlourArea;
