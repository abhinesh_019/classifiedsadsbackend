const mongoose=require('mongoose');

const classifiedsLandingPageSchema= new mongoose.Schema({

    catageries:String,
    images:String,
    classifiedsLocalArea:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifiedsLocalArea'
}
   
},
{timestamps:true});

const hostelsLocation=mongoose.model('classifiedsCatLocalAreas',classifiedsLandingPageSchema);
module.exports=hostelsLocation;