const mongoose=require('mongoose');

const classifiedsExecutives= new mongoose.Schema({

    name:String,
    mobileNo:String,
    emailID:String,
    images:String,
    description:String, 
    classifiedsExecutives:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifiedsLocalArea'
},
// classifiedsExecutivesFormainAreas:{
//     type:mongoose.Schema.Types.ObjectId,
//     ref:'classifiedsArea'
// }
},
{timestamps:true});

const catMainAreas=mongoose.model('classifiedsExicutives',classifiedsExecutives);
module.exports=catMainAreas;