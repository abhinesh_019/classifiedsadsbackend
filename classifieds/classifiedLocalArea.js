const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({

    localArea:String,
     
    classifiedsArea:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifiedsArea'
}
    
},
{timestamps:true});

const parlourArea=mongoose.model('classifiedsLocalArea',parlourSchemaAreas);
module.exports=parlourArea;

// 54.176.118.186