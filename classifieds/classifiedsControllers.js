
var animalsL = require('../classifieds/classifiedsLocations'); 
var localArea = require('../classifieds/classifiedLocalArea'); 
var landings = require('../classifieds/classifiedsCatageriesLocalAreas'); 
var catDetails = require('../classifieds/classifiedsDetails'); 

 var catMainAreas = require('../classifieds/classifiedsCatMainArea'); 

 var searchJob = require('../classifieds/classifiedsSerarchJobs');
var generals = require('../classifieds/classifiedsGeneralsDetails');
var classifiedsGets = require('../classifieds/classifiedsAreas');

var status = require('../classifieds/classifiedStatus');
var statgeneralslandings = require('../classifieds/classifiedsGeneralLandings');

var subCat = require('../classifieds/classifiedssubCat');
var optSub = require('../classifieds/classifiedOptSubCat');
 var dates = require('../classifieds/classifiedsDates');
 var detailsImg = require('../classifieds/classifiedsDetailsImg'); 
 var executives = require('../classifieds/classifiedsExicutives'); 
 var executivesFields = require('../classifieds/classifiedsExecutivesFields'); 
 var executivesCatImg = require('../classifieds/classifiedsDetailsCatImgs'); 
 var executivesCatVid = require('../classifieds/classifiedsDetailsCatVid'); 
 var statusOne = require('../classifieds/classifiedStatus'); 
 var statusTwo = require('../classifieds/classifiedsStatusTwo'); 
 var aboutUs = require('../classifieds/classifiedsAboutUs'); 
 var blogs = require('../classifieds/classifiedsBlogs'); 
var imp = require('../classifieds/classifiedsImpNotes'); 
var feedbacks = require('../classifieds/classifiedsFeedbacks'); 


const fileService = require('../classifieds/classifiedsFileServer');

var Mongoose=require("mongoose");
var ObjectId=Mongoose.Types.ObjectId;
var async = require('async')

//  var nodemailer = require('../helpers/nodemailer');
module.exports = {


// ********************ABOUT US *****************
 

clasdifiedsAboutUsPosts:(req, res, next) => {

    let allData = req.body;
  
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.image, function (err, data) {
                allData.image = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            aboutUs.create(allData, function (err, data) {

                console.log(this.allData, "this.alldata");


                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 
    });
},
 
clasdifiedsAboutUsget: async(req,res,next)=>{
    const users = await aboutUs.find({}).sort({"createdAt":-1})
       res.status(200).json(users);

   console.log(users,"users");
},
 
clasdifiedsAboutUsDelete: async (req, res, next) => {
    const { deleteId } = req.params;

    const result = await aboutUs.findByIdAndRemove(deleteId);

    res.status(200).json(result)
},

// ******************************************


//  *********************** IMPORTANT NOTES  **********
 

clasdifiedsImpNotesPosts:(req, res, next) => {

   
    let allData = req.body;
  
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.image, function (err, data) {
                allData.image = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            imp.create(allData, function (err, data) {

                console.log(this.allData, "this.alldata");


                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 
    });
},
 
clasdifiedsImpNotesget: async(req,res,next)=>{
    const users = await imp.find({}).sort({"createdAt":-1})
       res.status(200).json(users);

   console.log(users,"users");
},
 
clasdifiedsImpNotesDelete: async (req, res, next) => {
    const { deleteId } = req.params;

    const result = await imp.findByIdAndRemove(deleteId);

    res.status(200).json(result)
},

// ******************************************


//  *********************** Blogs  **********
 

clasdifiedsBlogsPosts:(req, res, next) => {

    let allData = req.body;
  
    async.parallel([

        function (callback) {

            fileService.uploadImage(req.files.image, function (err, data) {
                allData.image = data.Location;
                callback(null, data.Location);
            })
        },

 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            blogs.create(allData, function (err, data) {

                console.log(this.allData, "this.alldata");


                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }
            });
        }
 
    });
},
 
clasdifiedsBlogsget: async(req,res,next)=>{
    const users = await blogs.find({}).sort({"createdAt":-1})
       res.status(200).json(users);

 },
 
clasdifiedsBlogsDelete: async (req, res, next) => {
    const { deleteId } = req.params;

    const result = await blogs.findByIdAndRemove(deleteId);

    res.status(200).json(result)
},

// ******************************************



//  *********************** Feedbacks  **********
 
clasdifiedsFeedbacksPosts: async (req,res,next)=>{
    const locs=  req.body;
    await feedbacks.create(locs, function (err, data) {

 

        if (err) {
            console.log('Error in Saving user: ' + err);
        } else {

            res.status(201).json({ status: true, message: "user added sucessfully"});
        }

    });

},
clasdifiedsFeedbacksget: async(req,res,next)=>{
    const users = await feedbacks.find({}).sort({"createdAt":-1})
       res.status(200).json(users);

   console.log(users,"users");
},
 
clasdifiedsFeedbacksDelete: async (req, res, next) => {
    const { deleteId } = req.params;

    const result = await feedbacks.findByIdAndRemove(deleteId);

    res.status(200).json(result)
},

// ******************************************



    clasdifiedsGeneralsLocationsget: async (req, res, next) => {

        const users = await animalsL.aggregate([

           {$sort:{"locations":1}}
           
   ])

        res.status(200).json(users);
       console.log("users****", users);

   },

   clasdifiedsGeneralsLocationsPosts: async (req,res,next)=>{
    const locs=  req.body;
    await animalsL.create(locs, function (err, data) {

        console.log(this.locs, "this.alldata");


        if (err) {
            console.log('Error in Saving user: ' + err);
        } else {

            res.status(201).json({ status: true, message: "user added sucessfully"});
        }

    });

},

clasdifiedsGeneralsLocationsId:async (req, res, next) => {
     const { locationId } = req.params; 
     const usersposts = await animalsL.findById(locationId)
    res.status(200).json(usersposts);
 },


//  ************************ areas *******************
clasdifiedsGeneralsAreasPosts : async(req,res,next)=>{
    const { locations } = req.params;
    req.body.classifiedsLocation= locations
     const userscomm = new classifiedsGets(req.body);
    await userscomm.save(); 
       res.status(201).json(userscomm);
},

clasdifiedsGeneralsAreasget:async(req,res,next)=>{
const { locations } = req.params;
req.body.classifiedsLocation= locations
const users = await classifiedsGets.find({classifiedsLocation :locations}).sort({"area":1})
 res.status(200).json(users);
},
clasdifiedsGeneralsAreasGroupget:async(req,res,next)=>{
    const { locations } = req.params;
    
    const users = await landings.aggregate([{$match:{classifiedsLocation:ObjectId(locations)}},

        {"$group" : {_id:{area:"$area"},count:{$sum:1}}},{$sort:{"area":1}}
     ])
    
    res.status(200).json(users);
 
},
clasdifiedsGeneralsAreasgetId:async (req, res, next) => {
    const { locations } = req.params; 
    const usersposts = await classifiedsGets.findById(locations)
   res.status(200).json(usersposts);
},
// ******************************************************
// ************************************Local Area ****************************
 
clasdifiedsLocalAreasPosts : async(req,res,next)=>{
    const { AreaId } = req.params;
    req.body.classifiedsArea= AreaId
     const userscomm = new localArea(req.body);
    await userscomm.save(); 
       res.status(201).json(userscomm);
},

 

 
clasdifiedsLocalAreasget:async (req, res, next) => {

    const { AreaId } = req.params;
    

    const usersposts = await localArea.find({classifiedsArea:AreaId}).sort({"localArea":1})
    res.status(200).json(usersposts);
 
 },
 clasdifiedsLocalAreasdelete: async (req, res, next) => {
    const { AreaId } = req.params;

    const result = await localArea.findByIdAndRemove(AreaId);

    res.status(200).json(result)
},
clasdifiedsLocalAreasgetId:async (req, res, next) => {
    const { AreaId } = req.params; 
    const usersposts = await localArea.findById(AreaId)
   res.status(200).json(usersposts);
},
 
// *************************************************************************
// *******************************Local Areas catageries ******************************************
classifidesLandspost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifiedsLocalArea = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            landings.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
classifidesLandseget:async (req, res, next) => {
    const { localAreaId } = req.params;
    let search=req.query.search;

    const usersposts = await landings.find({classifiedsLocalArea:localAreaId,$or:[
        {catageries:new RegExp(search, "gi")},
      
    ]
}).sort({"catageries":1})
    res.status(200).json(usersposts);
 
 },
 
 classifidesCatageriesGroup:async (req, res, next) => {

    const { localAreaId } = req.params;
    
    const users = await landings.aggregate([{$match:{classifiedsLocalArea:ObjectId(localAreaId)}},

        {"$group" : {_id:{catageries:"$catageries"},count:{$sum:1}}},{$sort:{"catageries":1}}
     ])
    
    res.status(200).json(users);
 
},
 
 


 classifidesLandsdelete: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await landings.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},

classifidesLandsegetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await landings.findById(localAreaId)
   res.status(200).json(usersposts);


 
},

// *************************************************************************
// ******************************* Areas catageries ******************************************
classifidesCatMainAreaPost: async (req, res, next) => {
 
    const { MainAreaId } = req.params;
    req.body.classifiedsArea = MainAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
             if(req.files.images){
                fileService.uploadImage(req.files.images, function (err, data) {
                    allData.images = data.Location;
                    callback(null, data.Location);
                })
              }
            else{
                callback(null, null);   
            }
            
        },
        
        
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            catMainAreas.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
classifidesCatMainAreaGet:async (req, res, next) => {

    const { MainAreaId } = req.params;
    

    const usersposts = await catMainAreas.find({classifiedsArea:MainAreaId}).sort({"catageries":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesCatMainAreaGetId:async (req, res, next) => {
    const { MainAreaId } = req.params; 
    const usersposts = await catMainAreas.findById(MainAreaId)
   res.status(200).json(usersposts);
},

// *************************************************************************
// ******************************* Sub catageries ******************************************
classifidesSubCatPost: async(req,res,next)=>{
    const { catagiriesId } = req.params;
    req.body.classifiedsSubCat = catagiriesId
     const userscomm = new subCat(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesSubCatGet:async (req, res, next) => {

    const { catagiriesId } = req.params;
    

    const usersposts = await subCat.find({classifiedsSubCat:catagiriesId}).sort({"SubCatageries":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesSubCatGetId:async (req, res, next) => {
    const { catagiriesId } = req.params; 
    const usersposts = await catMainAreas.findById(catagiriesId)
   res.status(200).json(usersposts);
},
// *************************************************************************

// ******************************* Opt Sub catageries ******************************************
classifidesOptSubCatPost: async(req,res,next)=>{
    const { catagiriesId } = req.params;
    req.body.classifiedsOptSubCat = catagiriesId
     const userscomm = new optSub(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesOptSubCatGet:async (req, res, next) => {

    const { catagiriesId } = req.params;
    

    const usersposts = await optSub.find({classifiedsOptSubCat:catagiriesId}).sort({"optCatageries":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesOptSubCatGetId:async (req, res, next) => {
    const { catagiriesId } = req.params; 
    const usersposts = await optSub.findById(catagiriesId)
   res.status(200).json(usersposts);
},
// *************************************************************************

// ******************************* Dates ******************************************
 
classifidesDatepost: async(req,res,next)=>{
    const { catagiriesOpId } = req.params;
    req.body.classifiedsDates = catagiriesOpId
     const userscomm = new dates(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesDateget:async (req, res, next) => {

    const { catagiriesOpId } = req.params;
    

    const usersposts = await dates.find({classifiedsDates:catagiriesOpId}).sort({"dates":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesDategetId:async (req, res, next) => {
    const { catagiriesOpId } = req.params; 
    const usersposts = await dates.findById(catagiriesOpId)
   res.status(200).json(usersposts);
},
 
// *************************************************************************

// ******************************* catageries details ******************************************
classifidesDetailsDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await catDetails.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},

classifidesDetailspost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifidesDetails = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
            if(req.files.images1){
               fileService.uploadImage(req.files.images1, function (err, data) {
                   allData.images1 = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
         
        
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            catDetails.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
classifidesDetailsGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    let search=req.query.search;
    const usersposts = await catDetails.find({classifidesDetails:localAreaId,$or:[
    {name:new RegExp(search, "gi")},
    {mobileNo:new RegExp(search, "gi")}, 
    {area:new RegExp(search, "gi")},
    {colony:new RegExp(search, "gi")},
    {city:new RegExp(search, "gi")},
    {pincode:new RegExp(search, "gi")},
    {mainTitle:new RegExp(search, "gi")} 

 ]
}).sort({"colony":1,"pincode":1})
res.status(200).json(usersposts);

},
 classifidesDetailsGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await catDetails.findById(localAreaId)
   res.status(200).json(usersposts);
},
// *************************************************************************

// *******************************  Sub - Catageries - Details Status One ******************************************
 
classifidesStatusOnepost: async(req,res,next)=>{
    const { localAreaId } = req.params;
    req.body.classifidesStatusOne = localAreaId
     const userscomm = new statusOne(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesStatusOneGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await statusOne.find({classifidesStatusOne:localAreaId}).sort({"statusOne":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesStatusOneGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await statusOne.findById(localAreaId)
   res.status(200).json(usersposts);
},
classifidesStatusOneDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await statusOne.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},
// *************************************************************************


// *******************************  Sub - Catageries - Details Status Two ******************************************
 
classifidesStatusTwopost: async(req,res,next)=>{
    const { localAreaId } = req.params;
    req.body.classifidesStatusTwo = localAreaId
     const userscomm = new statusTwo(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesStatusTwoGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await statusTwo.find({classifidesStatusTwo:localAreaId}).sort({"statusTwo":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesStatusTwoGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await statusTwo.findById(localAreaId)
   res.status(200).json(usersposts);
},
classifidesStatusTwoDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await statusTwo.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},
// *************************************************************************


// ******************************* catageries details img ******************************************
classifidesDetailsImgDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await detailsImg.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},


classifidesDetailsImgpost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifidesDetailsImg = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
            if(req.files.images1){
               fileService.uploadImage(req.files.images1, function (err, data) {
                   allData.images1 = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
       
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            detailsImg.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
 

classifidesDetailsImgGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await detailsImg.find({classifidesDetailsImg:localAreaId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 
 },
 classifidesDetailsImgGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await detailsImg.findById(localAreaId)
   res.status(200).json(usersposts);
},
// *************************************************************************


// ******************************* Executives ******************************************
classifidesExecutivesDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await executives.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},


classifidesExecutivespost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifiedsExecutives = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
            if(req.files.images){
               fileService.uploadImage(req.files.images, function (err, data) {
                   allData.images = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
     
        
        
 
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            executives.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
 

classifidesExecutivesGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await executives.find({classifiedsExecutives:localAreaId}).sort({"createdAt":1})
    res.status(200).json(usersposts);
 
 },
 classifidesExecutivesGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await executives.findById(localAreaId)
   res.status(200).json(usersposts);
},
// *************************************************************************

// ******************************* Executives Fields ******************************************
classifidesExecutivesFieldsDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await executivesFields.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},
classifidesExecutivesFieldspost: async(req,res,next)=>{
    const { localAreaId } = req.params;
    req.body.classifiedsEfields = localAreaId
     const userscomm = new executivesFields(req.body);
    await userscomm.save();
     
       res.status(201).json(userscomm);
},

classifidesExecutivesFieldsGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await executivesFields.find({classifiedsEfields:localAreaId}).sort({"fields":1})
    res.status(200).json(usersposts);
 
 },
 classifidesExecutivesFieldsGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await executivesFields.findById(localAreaId)
   res.status(200).json(usersposts);
},
 
// *************************************************************************

// ******************************* catageries details catageries img ******************************************
classifidesCatImgGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await executivesCatImg.find({classifidesDetailsImg:localAreaId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 
 },

classifidesCatImgDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await executivesCatImg.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},


classifidesCatImgpost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifidesDetailsImg = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
            if(req.files.images){
               fileService.uploadImage(req.files.images, function (err, data) {
                   allData.images = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
       
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            executivesCatImg.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
 


 classifidesCatImgGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await executivesCatImg.findById(localAreaId)
   res.status(200).json(usersposts);
},
// *************************************************************************



// ******************************* catageries details catageries Vid ******************************************
classifidesCatVidGet:async (req, res, next) => {

    const { localAreaId } = req.params;
    

    const usersposts = await executivesCatVid.find({classifidesDetailsImg:localAreaId}).sort({"createdAt":-1})
    res.status(200).json(usersposts);
 
 },

 classifidesCatVidDel: async (req, res, next) => {
    const { localAreaId } = req.params;

    const result = await executivesCatVid.findByIdAndRemove(localAreaId);

    res.status(200).json(result)
},


classifidesCatVidpost: async (req, res, next) => {
 
    const { localAreaId } = req.params;
    req.body.classifidesDetailsImg = localAreaId
    let allData = req.body 

    async.parallel([


        function (callback) {
            if(req.files.videos){
               fileService.uploadImage(req.files.videos, function (err, data) {
                   allData.videos = data.Location;
                   callback(null, data.Location);
               })
             }
           else{
               callback(null, null);   
           }
           
       },
       
    ], function (err, result) {

        if (err) {
            consoler.err(err);
            return;
        } else {
            executivesCatVid.create(allData, function (err, data) {
 
                if (err) {
                    console.log('Error in Saving user: ' + err);
                } else {
                    console.log('User send succesful', allData);
                  res.status(201).json({ status: true, message: "user added sucessfully", data: allData });
                }  });  }  });
},
 
 


classifidesCatVidGetId:async (req, res, next) => {
    const { localAreaId } = req.params; 
    const usersposts = await executivesCatVid.findById(localAreaId)
   res.status(200).json(usersposts);
},
// *************************************************************************


}
