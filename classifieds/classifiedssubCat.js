const mongoose=require('mongoose');

const classifiedsSubCats= new mongoose.Schema({

    catageries:String,
    SubCatageries:String,

    classifiedsSubCat:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifiedsCatLocalAreas'
}
   
},
{timestamps:true});

const subCatsclassify=mongoose.model('classifiedsSubCat',classifiedsSubCats);
module.exports=subCatsclassify;