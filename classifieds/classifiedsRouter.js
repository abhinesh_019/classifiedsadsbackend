const express= require('express');
var router = express.Router();
const userControllers = require('../classifieds/classifiedsControllers');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

//  *********************** AboutUs **********
router.route('/clasdifiedsAboutUs')
.get(userControllers.clasdifiedsAboutUsget)
.post(multipartMiddleware,userControllers.clasdifiedsAboutUsPosts) 
router.route('/clasdifiedsAboutUs/:deleteId')
.delete(userControllers.clasdifiedsAboutUsDelete)

//  *********************** IMPORTANT NOTES  **********
router.route('/clasdifiedsImpNotes')
.get(userControllers.clasdifiedsImpNotesget)
.post(multipartMiddleware,userControllers.clasdifiedsImpNotesPosts) 
router.route('/clasdifiedsImpNotes/:deleteId')
.delete(userControllers.clasdifiedsImpNotesDelete)

//  *********************** Blogs  **********
router.route('/clasdifiedsBlogs')
.get(userControllers.clasdifiedsBlogsget)
.post(multipartMiddleware,userControllers.clasdifiedsBlogsPosts) 
router.route('/clasdifiedsBlogs/:deleteId')
.delete(userControllers.clasdifiedsBlogsDelete)

//  *********************** Feedbacks  **********
router.route('/clasdifiedsFeedbacks')
.get(userControllers.clasdifiedsFeedbacksget)
.post(userControllers.clasdifiedsFeedbacksPosts) 
router.route('/clasdifiedsFeedbacks/:deleteId')
.delete(userControllers.clasdifiedsFeedbacksDelete)


//  *********************** Locations **********
router.route('/clasdifiedsGeneralsLocations')
.get(userControllers.clasdifiedsGeneralsLocationsget)
.post(userControllers.clasdifiedsGeneralsLocationsPosts)
router.route('/clasdifiedsGeneralsLocationsId/:locationId')
.get(userControllers.clasdifiedsGeneralsLocationsId);

//  *********************** Main Areas **********

router.route('/clasdifiedsGeneralsAreas/:locations')
.get(userControllers.clasdifiedsGeneralsAreasget)
.post(userControllers.clasdifiedsGeneralsAreasPosts)
router.route('/clasdifiedsMainAreasGroup/:locations')
.get(userControllers.clasdifiedsGeneralsAreasGroupget)
router.route('/clasdifiedsGeneralsAreasID/:locations')
.get(userControllers.clasdifiedsGeneralsAreasgetId)
// *********************************************
// ********************* Local Areas ****************
router.route('/clasdifiedsLocalAreas/:AreaId')
.get(userControllers.clasdifiedsLocalAreasget)
.delete(userControllers.clasdifiedsLocalAreasdelete)
 .post(userControllers.clasdifiedsLocalAreasPosts)
 router.route('/clasdifiedsLocalAreasId/:AreaId')
.get(userControllers.clasdifiedsLocalAreasgetId)
// *********************************************
// ********************* Catageries - Main Areas****************
router.route('/classifidsCatMainArea/:MainAreaId')
.get(userControllers.classifidesCatMainAreaGet)
.get(userControllers.classifidesCatMainAreaGetId)
router.route('/classifidsCatMainAreap/:MainAreaId')
 .post(multipartMiddleware,userControllers.classifidesCatMainAreaPost)
//  ********************************************
 // ********************* Catageries - Local Areas****************
 router.route('/classifidesCatlocalArea/:localAreaId')
.get(userControllers.classifidesLandseget)
.post(multipartMiddleware,userControllers.classifidesLandspost)
.delete(userControllers.classifidesLandsdelete)

 
router.route('/classifidesCatGroup/:localAreaId')
.get(userControllers.classifidesCatageriesGroup)
router.route('/classifidesCatlocalAreaId/:localAreaId')
.get(userControllers.classifidesLandsegetId)

// *********************************************
// ********************* Sub - Catageries  ****************
 router.route('/classifidsSubCat/:catagiriesId')
 .get(userControllers.classifidesSubCatGet)
   .post(userControllers.classifidesSubCatPost)
 router.route('/classifidsSubCatId/:catagiriesId')
 .get(userControllers.classifidesSubCatGetId)

 // ********************* Sub - Catageries - Options  ****************
  router.route('/classifidsOptSubCat/:catagiriesId')
   .get(userControllers.classifidesOptSubCatGet)
     .post(userControllers.classifidesOptSubCatPost)
     router.route('/classifidsOptSubCatId/:catagiriesId')
     .get(userControllers.classifidesOptSubCatGetId)

 // ********************* Sub - Catageries - Dates  ****************

router.route('/classifidesDate/:catagiriesOpId')
.get(userControllers.classifidesDateget)
  .post(userControllers.classifidesDatepost);
  router.route('/classifidesDateId/:catagiriesOpId')
  .get(userControllers.classifidesDategetId)

 // ********************* Sub - Catageries - Details  ****************

  router.route('/classifidesDetails/:localAreaId')
  .get(userControllers.classifidesDetailsGet)
  .delete(userControllers.classifidesDetailsDel)
  .post(multipartMiddleware,userControllers.classifidesDetailspost)
  router.route('/classifidesDetailsId/:localAreaId')
  .get(userControllers.classifidesDetailsGetId)


 // ********************* Sub - Catageries - Details Status One****************

 router.route('/classifidesStatusOne/:localAreaId')
 .get(userControllers.classifidesStatusOneGet)
 .delete(userControllers.classifidesStatusOneDel)
 .post(userControllers.classifidesStatusOnepost)
 router.route('/classifidesStatusOneId/:localAreaId')
 .get(userControllers.classifidesStatusOneGetId)

  // ********************* Sub - Catageries - Details Status Two****************

  router.route('/classifidesStatusTwo/:localAreaId')
  .get(userControllers.classifidesStatusTwoGet)
  .delete(userControllers.classifidesStatusTwoDel)
  .post(userControllers.classifidesStatusTwopost)
  router.route('/classifidesStatusTwoId/:localAreaId')
  .get(userControllers.classifidesStatusTwoGetId)

 
    // ********************* Sub - Catageries - Details - Executives  ****************

    router.route('/classifidesExecutives/:localAreaId')
    .get(userControllers.classifidesExecutivesGet)
    .delete(userControllers.classifidesExecutivesDel)
    .post(multipartMiddleware,userControllers.classifidesExecutivespost)
    router.route('/classifidesExecutivesId/:localAreaId')
    .get(userControllers.classifidesExecutivesGetId)
        // ********************* Sub - Catageries - Details - Executives Fields  ****************

        router.route('/classifidesExecutivesFields/:localAreaId')
        .get(userControllers.classifidesExecutivesFieldsGet)
        .delete(userControllers.classifidesExecutivesFieldsDel)
        .post(userControllers.classifidesExecutivesFieldspost)
        router.route('/classifidesExecutivesFieldsId/:localAreaId')
        .get(userControllers.classifidesExecutivesFieldsGetId)

  // ********************* Sub - Catageries - Details -img  ****************

  router.route('/classifidesDetailsImg/:localAreaId')
  .get(userControllers.classifidesDetailsImgGet)
  .delete(userControllers.classifidesDetailsImgDel)
  .post(multipartMiddleware,userControllers.classifidesDetailsImgpost)
  router.route('/classifidesDetailsImgId/:localAreaId')
  .get(userControllers.classifidesDetailsImgGetId)

  // ********************* Sub - Catageries - Details -img  ****************

  router.route('/classifidesCatImg/:localAreaId')
  .get(userControllers.classifidesCatImgGet)
  .delete(userControllers.classifidesCatImgDel)
  .post(multipartMiddleware,userControllers.classifidesCatImgpost)
  router.route('/classifidesDetailsImgId/:localAreaId')
  .get(userControllers.classifidesCatImgGetId)

  // ********************* Sub - Catageries - Details -Vid  ****************

  router.route('/classifidesCatVid/:localAreaId')
  .get(userControllers.classifidesCatVidGet)
  .delete(userControllers.classifidesCatVidDel)
  .post(multipartMiddleware,userControllers.classifidesCatVidpost)
  router.route('/classifidesDetailsImgId/:localAreaId')
  .get(userControllers.classifidesCatVidGetId)

 module.exports = router;