const mongoose=require('mongoose');

const classifiedsoSubCats= new mongoose.Schema({

    optCatageries:String,
    SubCatageries:String,

    classifiedsOptSubCat:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifiedsSubCat'
}
   
},
{timestamps:true});

const subCatsclassifys=mongoose.model('classifiedsOptSubCat',classifiedsoSubCats);
module.exports=subCatsclassifys;