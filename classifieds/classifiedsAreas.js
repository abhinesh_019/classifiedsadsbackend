const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({

    area:String,
    
    classifiedsLocation:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'clasdifiedsGeneralslocations'
}
    
},
{timestamps:true});

const parlourArea=mongoose.model('classifiedsArea',parlourSchemaAreas);
module.exports=parlourArea;