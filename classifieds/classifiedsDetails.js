const mongoose=require('mongoose');

const classifiedsLandingPageSchema= new mongoose.Schema({

    name:String,
    mobileNo:String,
    emailid:String,
    whatsApp:String,
    Hno:String,
    streetNo:String,
    colony:String,
    area:String,
    city:String,
    pincode:String,
    mainTitle:String,
    subTitle:String,
    description:String,
        images1:String,
    
    classifidesDetails:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'classifidesDate'
},
 
},
{timestamps:true});

const hostelsLocation=mongoose.model('classifiedDetails',classifiedsLandingPageSchema);
module.exports=hostelsLocation;