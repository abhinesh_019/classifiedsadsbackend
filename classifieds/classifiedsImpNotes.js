const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({

    name:String,
    mobileNo:String,
    whatsapp:String,
     mainTitle:String,
    subtitle:String,
    description:String, 
    image:String
},
{timestamps:true});

const parlourArea=mongoose.model('classifiedsImpNotes',parlourSchemaAreas);
module.exports=parlourArea;
