const mongoose=require('mongoose');

const parlourSchemaAreas= new mongoose.Schema({

    name:String,
    mobileNo:String,
    whatsapp:String,
    emailid:String,
    address:String,
    fb:String,
    inst:String,
    youtube:String,
    mainTitle:String,
    subtitle:String,
    description:String,
    mainTitle1:String,
    subtitle1:String,
    description1:String,
    mainTitle2:String,
    description2:String,
    subtitle2:String,
    mainTitle3:String,
    subtitle3:String,
    description3:String,
image:String
},
{timestamps:true});

const parlourArea=mongoose.model('classifiedsAboutUs',parlourSchemaAreas);
module.exports=parlourArea;
