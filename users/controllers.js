const express = require('express');
var User = require('./models');
 var mongoose = require('mongoose');
var utils = require('../helpers/util');
var bcrypt = require('bcryptjs');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var config = require('../config');
var middleware = require('../middleware/middleware')
var mailer = require('../helpers/mailers');
var User = require('./models');
var nodemailers = require('../helpers/nodemailer')
let register = (req, res, next) => {
   


    User.find({ email: req.body.email })

        .then(user => {
            if (user.length >= 1) {
                return res.status(422).json({
                    message: "mail already exists"
                });
            } else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.sendStatus(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash,
                            role: req.body.role,
                            contactNo: req.body.contactNo,
                            userName: req.body.userName,
                             adminsId:req.body.adminsId
                        });
                        user
                            .save()
                            .then(result => {
                                console.log(result);

                                res.status(200).json({
                                    message: 'user Control sussess',
                                    result: result
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error: err
                                });

                            });
                    };
                });
            };
        });

} 
// 104510100105375
// 83034134
let login = (req, res, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    message: 'login failed'
                });
            }

            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        message: 'login failed'
                    });
                }
                console.log("kyss", config)
                if (result) {
                    const token = jwt.sign({
                        email: user[0].email,
                        userId: user[0]._id,
                        role: user[0].role,
                        adminsId:user[0].adminsId,
                         contactNo: user[0].contactNo,
                        userName: user[0].userName
                     },
                        config.jwt.key,
                        {
                            expiresIn: config.jwt.expiresIn
                        }
                    )
                    return res.status(200).json({
                        message: 'login was successfull',
                        token: token,
                        email: user[0].email,
                        userId: user[0]._id,
                        role: user[0].role,
                        adminsId:user[0].adminsId,
                         contactNo: user[0].contactNo,
                        userName: user[0].userName
                    });
                }

                res.status(401).json({
                    message: 'login was failed',

                });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });

        });

}
let deleteuser = (req, res, next) => {
    User.remove({ _id: req.params.userId })
        .exec()
        .then(result => {
            res.status(200).json({
                message: "users was deleted"
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });

        });

}

 

// let profile = function (req, res) {
 
//     const users = await blogs.find({}).sort({"createdAt":-1})
//     res.status(200).json(users);

// }
let loginId = (req, res, next) => {
    User.find({ _id: req.params.login })
        .exec()
        .then(result => {
            console.log(result);

            res.status(200).json({
                message: " users was found by login "
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};



let forgetpasswords = (req, res) => {
     let email = req.body.email;
    User.findOne({ email: email })

        .then(response => {

            if (!response) res.status(400).json({ status: false, message: 'User not found', data: null });

            else {

                let passwordResetLink = nodemailers.generatePasswordResetLinkForUser(email);
 
                nodemailers.sendPasswordResetLinkToUser(email, response.userName, passwordResetLink);

                res.status(200).send({ status: true, message: "reset password link sent", data: null });
                console.log(res)
            }
        }).catch(err => {
            console.log(err);
            res.status(500).json({ status: false, message: 'Internal server error', data: null });
        });
 
}


let resetPasswordWithToken = (req, res) => {
    let resetToken = req.body.token;
    nodemailers.decodePasswordResetToken(resetToken)
        .then(data => {
            let email = data.email;
            console.log(email);
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.sendStatus(500).json({
                        error: err
                    });
                } else {
                    req.body.password = hash;
                    Promise.all([
                        User.findOneAndUpdate({ email }, { password: req.body.password, updatedDate: Date.now() }),
                        User.findOne({ email })])
                        .then(([response, user]) => {
                            console.log(response);
                            nodemailers.sendPasswordResetSuccessIntimationToUser(email, response.userName);
                            res.status(200).send({ status: true, message: "Password changed successfully", data: null });
                        }).catch(err => {
                            console.log("errpr", err)
                            res.status(500).send({ status: false, message: "Intenal server error", data: null });
                        });
                }
            })

        }).catch(err => {
            console.log("error", err)
            res.status(400).send({ status: false, message: "Sdcsd" + err, data: null });
        });
}




module.exports = {
    register,
    login,
    deleteuser,
    // profile,
    loginId,
    forgetpasswords,
    resetPasswordWithToken,
    // resetPasswordWithToken

    profile: async(req,res,next)=>{
        const users = await User.find({}).sort({"createdAt":-1})
           res.status(200).json(users);
    
     },
    // profile: async(req,res,next)=>{
    //     let search=req.query.search;
    
    //     const usersposts = await landings.find({emai:emai,$or:[
    //         {role:new RegExp(search, "gi")},
    //         {email:new RegExp(search, "gi")},
    //         {contactNo:new RegExp(search, "gi")},
    //         {userName:new RegExp(search, "gi")},
    //         {adminsId:new RegExp(search, "gi")},
        
    //     ]
    //     }).sort({"userName":1})
    //     res.status(200).json(usersposts);
    
    //  },
}

 

