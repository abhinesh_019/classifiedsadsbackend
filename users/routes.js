const express = require('express');
var router = express.Router();
var middleware = require('../middleware/middleware')
let UsersController = require('../users/controllers');


router.post('/singup',UsersController.register),
router.post('/login',UsersController.login),
router.delete('/:userId',UsersController.deleteuser),
router.get('/profile',UsersController.profile),
router.get('/login/:login',UsersController.loginId),
router.post('/forgetPassword',UsersController.forgetpasswords),

// router.get('/forgotpassword',UsersController.forgotpasswords),
router.post('/resetPasswordWithToken',UsersController.resetPasswordWithToken),
module.exports = router;



