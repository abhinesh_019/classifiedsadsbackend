const mongoose=require('mongoose');
const userSchema=mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    email:{type:String,unique:true,
           match:/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
        
        },
        
    role:{type:String,default:"normal",enum:['normal','admin'],required:true},
    password:{type:String},
    contactNo:{type:String,unique:true},
    userName:{type:String,unique:true},
    adminsId:{type:String,unique:true}
});

module.exports=mongoose.model('User',userSchema);
